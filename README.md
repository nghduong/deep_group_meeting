# 
Nguyen Hai Duong (nhduong_3010@live.com)  
Chonnam National University  
  
## Requirements
1. Windows 7/10 Pro x64
2. Anaconda 4.2 Python 3.5
3. TensorFlow 1.1.0 with Keras interface
4. Jupyter IDE
5. CUDA 8.0
6. cuDNN v5